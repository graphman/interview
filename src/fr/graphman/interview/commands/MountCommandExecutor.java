package fr.graphman.interview.commands;

import fr.graphman.interview.InterviewPlugin;
import fr.graphman.interview.entities.ControllableIronGolem;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;
import org.bukkit.entity.Player;

/**
 * @author graphman
 */

public class MountCommandExecutor implements CommandExecutor {

    private InterviewPlugin plugin;

    public MountCommandExecutor(InterviewPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "Only players can perform this command.");
            return false;
        }

        Player player = (Player) commandSender;
        Location location = player.getLocation();

        CraftWorld world = (CraftWorld)location.getWorld();

        ControllableIronGolem golem = new ControllableIronGolem(world.getHandle(), location);

        world.getHandle().addEntity(golem);

        golem.getBukkitEntity().setPassenger(player);

        plugin.customEntities.add(golem.getBukkitEntity());

        return true;
    }
}
