package fr.graphman.interview.entities;

import net.minecraft.server.v1_9_R1.*;
import org.bukkit.Location;

import java.lang.reflect.Field;

/**
 * @author graphman
 */

public class ControllableIronGolem extends EntityIronGolem {

    public ControllableIronGolem(World world, Location location) {
        super(world);
        this.setPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    @Override
    public void g(float sideMotion, float forwardMotion) {
        if(this.passengers.size() == 0 || !(this.passengers.get(0) instanceof EntityHuman)) {
            super.g(sideMotion, forwardMotion);
            return;
        }

        EntityHuman controller = (EntityHuman) this.passengers.get(0);

        this.lastYaw = this.yaw = controller.yaw;
        this.pitch = controller.pitch * 0.5F;
        this.setYawPitch(this.yaw, this.pitch);
        this.aO = this.aM = this.yaw;
        sideMotion = controller.bd * 0.5F;
        forwardMotion = controller.be;

        if(forwardMotion <= 0.0F)
            forwardMotion *= 0.25F;

        this.P = 1.0F;
        this.l((float)this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).getValue());
        super.g(sideMotion, forwardMotion);


        try {
            Field jump = EntityLiving.class.getDeclaredField("bc");
            jump.setAccessible(true);
            if(jump != null && this.onGround) {
                if(jump.getBoolean(controller)) {
                    this.motY = 0.5D;
                }
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

}
