package fr.graphman.interview.utils;

import org.bukkit.Location;

import java.util.HashSet;
import java.util.Set;

/**
 * @author graphman
 */

public class DomeGenerator {

    public static final int DEFAULT_THICKNESS = 1;
    public static final int DEFAULT_PRECISION = 2;

    public static Set<Location> GenerateDomeBlockLocations(Location origin, int radius, int thickness,
                                                           int precision, boolean checkSolid) {
        HashSet<Location> locations = new HashSet<>();
        for (int z = -radius + 1 - precision; z < radius + precision; z++) {
            for (int y = -radius + 1 - precision; y < radius + precision; y++) {
                for (int x = -radius + 1 - precision; x < radius + precision; x++) {
                    double distance = Math.sqrt(x * x + y * y + z * z);
                    if (Math.abs(distance - radius) <= thickness) {
                        int cx = origin.getBlockX() + x;
                        int cy = origin.getBlockY() + y;
                        int cz = origin.getBlockZ() + z;
                        if (checkSolid) {
                            if (origin.getWorld().getBlockAt(cx, cy, cz).getType().isSolid()) continue;
                        }
                        locations.add(new Location(origin.getWorld(), cx, cy, cz));
                    }
                }
            }
        }

        return locations;
    }

    public static Set<Location> GenerateDomeBlockLocations(Location origin, int radius, boolean checkSolid) {
        return DomeGenerator.GenerateDomeBlockLocations(origin, radius, DEFAULT_THICKNESS, DEFAULT_PRECISION, checkSolid);
    }
}
