package fr.graphman.interview.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * @author graphman
 */

public class BlockListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();

        if(event.getBlock().getType().equals(Material.IRON_BLOCK) &&
                player.getInventory().getItemInMainHand().getType().equals(Material.GOLD_PICKAXE)) {
            player.performCommand("mount");
        }
    }
}
