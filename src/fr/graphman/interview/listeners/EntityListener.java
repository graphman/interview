package fr.graphman.interview.listeners;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author graphman
 */

public class EntityListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDie(EntityDeathEvent event) {
        LivingEntity killed = event.getEntity();

        if(killed.getType().equals(EntityType.ENDERMAN) &&  killed.getKiller() != null) {
            Player killer = killed.getKiller();

            ItemStack diamondSword = new ItemStack(Material.DIAMOND_SWORD);
            diamondSword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
            diamondSword.addEnchantment(Enchantment.DURABILITY, 2);

            killer.getInventory().addItem(diamondSword);

            World nether = killer.getServer().getWorld("world_nether");

            killer.teleport(nether.getSpawnLocation());
        }
    }
}
