package fr.graphman.interview.listeners;

import fr.graphman.interview.utils.DomeGenerator;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author graphman
 */

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.sendMessage("Bonjour " + player.getName() + ", bienvenue sur le serveur IceCube");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {

        Action action = event.getAction();

        if((action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) &&
                event.getItem() != null && event.getItem().getType().equals(Material.STICK)) {

            Player player = event.getPlayer();

            World world = player.getWorld();
            world.playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_DEATH, 0.5F, 1.0F);

            for(Location loc : DomeGenerator.GenerateDomeBlockLocations(player.getLocation(), 7, true)) {
                loc.getBlock().setType(Material.ICE);
                world.playEffect(loc, Effect.EXPLOSION_LARGE, null);
            }

        }
    }

}
