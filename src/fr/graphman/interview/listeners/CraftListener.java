package fr.graphman.interview.listeners;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author graphman
 */

public class CraftListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBeforeCraft(PrepareItemCraftEvent event) {

        ItemStack currentlyCrafted = event.getRecipe().getResult();

        if(currentlyCrafted.getType().equals(Material.WOOD_PICKAXE)) {

            ItemStack enchantedDiamondPickaxe = new ItemStack(Material.DIAMOND_PICKAXE);
            enchantedDiamondPickaxe.addEnchantment(Enchantment.DURABILITY, 2); // Unbreakable II
            enchantedDiamondPickaxe.setAmount(currentlyCrafted.getAmount());

            event.getInventory().setResult(enchantedDiamondPickaxe);

        }
    }
}
