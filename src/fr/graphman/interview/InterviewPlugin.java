package fr.graphman.interview;

import fr.graphman.interview.commands.MountCommandExecutor;
import fr.graphman.interview.entities.ControllableIronGolem;
import fr.graphman.interview.listeners.BlockListener;
import fr.graphman.interview.listeners.CraftListener;
import fr.graphman.interview.listeners.EntityListener;
import fr.graphman.interview.listeners.PlayerListener;
import fr.graphman.interview.utils.EntityRegistrator;
import net.minecraft.server.v1_9_R1.EntityIronGolem;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * @author graphman
 */

public class InterviewPlugin extends JavaPlugin {

    public List<Entity> customEntities;

    @Override
    public void onEnable() {
        this.customEntities = new ArrayList<>();

        this.getCommand("mount").setExecutor(new MountCommandExecutor(this));

        this.getServer().getPluginManager().registerEvents(new CraftListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        this.getServer().getPluginManager().registerEvents(new BlockListener(), this);
        this.getServer().getPluginManager().registerEvents(new EntityListener(), this);

        EntityRegistrator.registerEntity("ControllableIronGolem", 99, EntityIronGolem.class, ControllableIronGolem.class);
    }

    @Override
    public void onDisable() {
        for(Entity entity : this.customEntities) {
            entity.remove();
        }
    }

}
